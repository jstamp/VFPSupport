FMA testbed
===========

Common notes
------------
First, copy in the source files
  CMath.s
  Fused32.s
  Fused64.s
  Macros.s
from the VFPSupport sources. They are not duplicated here.

Where doubles are used, remember that Norcroft C currently targets only the FPA coprocessor so orders the 2 words backwards compared with the VFP coprocessor.

Also, the calling convention will expend effort moving things from FPA registers (which are emulated by FPEmulator) into integer registers and back again. In order to avoid this various nefarious casting is used to make things look like uint64_t's and thus avoid FPEmulator having to decode and move values in and out of the virtual register bank.

Tests are run twice, first for 32 bit floats then for 64 bit doubles.

Timing mode
-----------
When the TIMING define is 1.

This runs a single set of (nonzero) inputs through
a) The VFMA instruction, implemented in silicon (eg. on a Cortex-A15)
b) An emulation of VFMA written in VFP
c) The CLib implementation written in FPA and integer maths
d) A dummy empty function which takes the same arguments

The empty loop (d) allows the function calling overheads to be subtracted.

Accuracy mode
-------------
When the TIMING define is 0.

This builds a big table of input values which are designed to exercise the full dynamic range of a floating point number for all valid inputs including �0. Appended to the table are the odd values for infinity and NaN.

These are then brute force applied to
a) The VFMA instruction, implemented in silicon (eg. on a Cortex-A15)
b) An emulation of VFMA written in VFP

The results are compared and any non-bit identical results reported and counted. At the end of the test there should be 0 diffs.
