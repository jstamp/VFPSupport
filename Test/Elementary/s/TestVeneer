;
; Copyright (c) 2021 RISC OS Open Limited
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:FSNumbers
        GET     Hdr:NewErrors
        GET     Hdr:VFPSupport

        AREA    |test$$Code|, CODE, READONLY, PIC
        ARM

        MACRO
        FPAOp32 $op, $proto, $args
        ; Calls float proto(float [, float])
        EXPORT  fpaop32_$proto
fpaop32_$proto
      [ "$args" = "rr"
        Push    "r0, r1"
        LDFS    f0, [sp], #4
        LDFS    f1, [sp], #4
        [ "$op" = "POL"
        $op.S   f0, f1, f0              ; Args reversed
        |
        $op.S   f0, f0, f1
        ]
      |
        Push    "r0"
        LDFS    f0, [sp], #4
        $op.S   f0, f0
      ]
        STFS    f0, [sp, #-4]!
        Pull    "r0"
        MOV     pc, lr
        MEND

        MACRO
        FPAOp64 $op, $proto, $args
        ; Calls double proto(double [, double])
        EXPORT  fpaop64_$proto
fpaop64_$proto
      [ "$args" = "rr"
        SUB     sp, sp, #4*4
        STR     r0, [sp, #4]            ; Note FPA word order swap
        STR     r1, [sp, #0]
        STR     r2, [sp, #12]           ; Note FPA word order swap
        STR     r3, [sp, #8]
        LDFD    f0, [sp], #8
        LDFD    f1, [sp], #8
        [ "$op" = "POL"
        $op.D   f0, f1, f0              ; Args reversed
        |
        $op.D   f0, f0, f1
        ]
      |
        Push    "r0"
        Push    "r1"
        LDFD    f0, [sp], #8
        $op.D   f0, f0
      ]
        STFD    f0, [sp, #-8]!
        Pull    "r1, r2"
        MOV     r0, r2                  ; Swap back
        MOV     pc, lr
        MEND

        MACRO
        VFPTranscendental32 $proto, $args
        ; Calls float proto(float [, float])
        EXPORT  vfpfn32_$proto
vfpfn32_$proto
        VMOV    s0, r0
      [ "$args" = "rr"
        VMOV    s1, r1
      ]
        Push    "lr"
        LDR     ip, vfp_ptr32
        LDR     ip, [ip]
        ADD     ip, ip, #VFPSupport_Fn_$proto :SHL: 2
        BLX     ip
        VMOV    r0, s0
        Pull    "pc"
        MEND

        MACRO
        VFPTranscendental64 $proto, $args
        ; Calls double proto(double [, double])
        EXPORT  vfpfn64_$proto
vfpfn64_$proto
        VMOV    d0, r0, r1
      [ "$args" = "rr"
        VMOV    d1, r2, r3
      ]
        Push    "lr"
        LDR     ip, vfp_ptr64
        LDR     ip, [ip]
        ADD     ip, ip, #VFPSupport_Fn_$proto :SHL: 2
        BLX     ip
        VMOV    r0, r1, d0
        Pull    "pc"
        MEND

        MACRO
        Veneer  $op, $proto, $args
        FPAOp32 $op, $proto, $args
        FPAOp64 $op, $proto, $args
        VFPTranscendental32 $proto, $args
        VFPTranscendental64 $proto, $args
        MEND

        ; The elementary functions
        Veneer  SIN, sin                ; Trig
        Veneer  COS, cos                ; Trig
        Veneer  TAN, tan                ; Trig
        Veneer  ASN, asin               ; ArcTrig
        Veneer  ACS, acos               ; ArcTrig
        Veneer  ATN, atan               ; ArcTrig
        Veneer  POL, atan2, rr          ; ArcTrig
        Veneer  EXP, exp                ; Power
        Veneer  POW, pow, rr            ; Power
        Veneer  LGN, log                ; Power
        Veneer  LOG, log10              ; Power

        EXPORT  fpa_fpsr
fpa_fpsr
        ; fpa_fpsr(bicmask, ormask)
        ; returns old value
        RFS     r2
        BIC     r0, r2, r0
        ORR     r0, r0, r1
        WFS     r0
        MOV     r0, r2
        MOV     pc, lr

        IMPORT  vfp_fns32
        IMPORT  vfp_fns64
vfp_ptr32 DCD   vfp_fns32
vfp_ptr64 DCD   vfp_fns64

        EXPORT  vfp_fpscr
vfp_fpscr
        ; vfp_fpscr(bicmask, ormask)
        ; returns old value
        VMRS    r2, FPSCR
        BIC     r0, r2, r0
        ORR     r0, r0, r1
        VMSR    FPSCR, r0
        MOV     r0, r2
        MOV     pc, lr

        END
